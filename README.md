# 工程简介
**springboot 轻量级配置中心**

> 配置中心现有的实现方式有很多，例如SpringCloud生态微服务组件SpringCloud Config、Alibaba开源Nacos框架、携程开源框架Apollo、百度开源配置中心Disconf 等，但对于中小型项目来说，**上述方案要么需要整个项目采用spring cloud微服务架构，要么需要考虑配置中心的服务稳定问题（集群部署费时费力），并且有一定的代码侵入不方便未来切换**

对于中小型项目来说，上述方案虽然很成熟完善但需要耗费一定的时间和成本。而leightweight配置中心能够在满足基本需求的同时尽量保持"轻量"，主要带来如下特性：
1. 考虑到配置中心的稳定性，采用git私有仓库或者DB来存储无论在成本方面以及稳定性（RDS）、安全性方面都有极大的优势
2. 代码无侵入或者只需要极少的适配代码，继续使用spring @Value注入配置值，同时又不必大量改写现有的项目代码
3. 动态配置，无须重启项目自动热更新

可以发现，lightweight-config 就是在不使用SpringCloud的前提下，拥有SpringCloud Config的优良特性（最终目标🐶） 

# 实现原理
基于spiringboot配置拓展EnvironmentPostProcessor在容器初始化之前进行远程配置的加载注入，同时结合一系列aware回调以及反射实现配置热加载

配置刷新采用定时任务拉取的方式，通过比对version以及lastUpdateTime时间戳判断配置信息是否更新从而决定是否进行配置替换  

本项目涉及Bean的生命周期等Spring源码知识，用来巩固知识储备也是不错的选择

# 使用方式
0. 当前项目并没有发布到中央仓库，所以需要下载本项目到本地，执行`mvn -U clean install -DskipTest`将jar包安装到本地仓库
1. 引入starter依赖
```xml
<dependency>
    <groupId>com.youcheng.configcenter</groupId>
    <artifactId>lightweight-config-spring-boot-starter</artifactId>
    <version>1.0.0-SNAPSHOT</version>
</dependency>
```
2. 根据远程配置采用的存储方式，配置git access_token或者数据库链接信息，采用DB （Mysql）时，需要先执行 mysql-schema.sql 建表

```yaml
# mysql 存储（mysql、gitee二选一）
lightweight:
  config:
    provider: com.youcheng.configcenter.provider.DataBaseConfigProvider
    data-base:
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306/config_center
      username: root
      password: mysql123
    configfile:
      id: abcdefg
      profile: local

# gitee 存储（mysql、gitee二选一）
lightweight:
  config:
    provider: com.youcheng.configcenter.provider.GiteeConfigProvider
    gitee:
      url: https://gitee.com/api/v5/repos/hongmin_shm/springboot-config-center/contents/application.properties
      token: 5d6d60f188c3bf790c180dbfb56ff305

# 开启定时任务刷新
     refresh:
       enable: true # 是否开启配置刷新，默认false
       delay: 5 # 刷新间隔，单位秒
```
# 快速开始使用
可参考客户端工程快速配置使用，同时提供一套REST API操作配置（需选择远程配置为mysql）
https://gitee.com/hongmin_shm/leightweight-config-client-demo

# gitee 配置方法
1. 官网注册gitee账号（如果有账号可以跳过）  
2. 建一个专门存储配置文件的git仓库（配置信息敏感请设置仓库为私有），新建一个application.properties 配置文件并push  
3. 点击头像->设置  
![setting](static/img.png)
安全设置-> 私人令牌，生成令牌（即access_token)  
![access_token](static/img_1.png)
4. 参考gitee openapi [获取仓库具体路径下的内容](https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepoContents%28Path%29) 配置一个访问application.properties的http链接，可以点击页面下方的测试按钮查看是否配置成功    

例如：https://gitee.com/api/v5/repos/hongmin_shm/springboot-config-center/contents/application.properties  
![gitee devepoler](static/20221023170052.jpg)  
5. 将4中生成的链接以及3中生成access_token 配置到引入配置中心的工程中，启动项目即可进行测试  
![测试](static/20221024230039.jpg)

# 配置文件表结构设计
```sql
CREATE TABLE `application_config_center_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `file_id` varchar(64) NOT NULL COMMENT '文件ID',
  `name` varchar(64) NOT NULL COMMENT '文件名',
  `profile` varchar(64) NOT NULL COMMENT '环境',
  `extension` varchar(16) NOT NULL COMMENT '后缀（properties、yaml、json等)',
  `content` text NOT NULL COMMENT '配置内容',
  `last_updated_time` datetime(6) NOT NULL COMMENT '最后更新时间',
  `last_updated_user` varchar(32) DEFAULT 'system' COMMENT '最后更新操作人',
  `version` int NOT NULL COMMENT '版本',
  PRIMARY KEY (`id`),
  KEY `idx` (`file_id`,`profile`,`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='应用配置中心信息表';
```

# 关于定时任务
由于目前配置更新是采用定时任务拉取，所以时间间隔是一个需要在效率和实时方面进行权衡，为了尽量减少系统资源的浪费，对于DB存储的方式，定时任务会先比对version是否有变更以及对比lastUpdatedTime时间戳，尽量在有必要的情况再进行配置的热更新  
如有更好的实现方式，例如怎样对配置进行监听，主动推送配置更新事件来通知使用方进行实时更新，欢迎提pr  

# 拓展
1. 自定义远程配置加载方式
当前支持git以及DB存储配置，也可以拓展为其他存储方式例如本地文件，参考com.youcheng.configcenter.provider.RemoteConfigProvider
2. 自定义文件格式解析
当前支持properties、json、yaml(尚不完善)格式的文本格式解析，可以拓展其他格式，参考com.youcheng.configcenter.convertor.ResourceConvertor

欢迎pr、issu、讨论  
个人联系方式：(wechat)15043284139 (email)hongmin.shm@aliyun.com
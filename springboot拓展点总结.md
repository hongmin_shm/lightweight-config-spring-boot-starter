**org.springframework.context.ApplicationContextInitializer#initialize**
整个spring容器在刷新之前调用此类的initialize方法，可以加载配置，动态字节码注入等操作
生效条件：1. spring.factories 2. 配置文件context.initializer.classes 3.启动类application.addInitializers(new xxxContextInitializer())

**org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor**
读取BeanDefinition之后执行，可以动态注册自己的beanDefinition ,加载classpath之外的bean

**org.springframework.beans.factory.config.BeanFactoryPostProcessor**
读取BeanDefinition之后，实例化bean之前，可以修改已经注册的BeanDefinition的元信息

**org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor**
集成BeanPostProcess接口，区别如下
BeanPostProcess 接口只在bean的初始化阶段进行拓展（注入spring上下文前后）
InstantiationAwareBeanPostProcessor 接口在此基础上增加3个方法，把可拓展的范围增加了实例化阶段和属性注入阶段
该接口有5个方法，分为实例化阶段和初始化阶段，调用顺序为
postProcessBeforeInstantiation：实例化Bean之前，相当于new这个bean之前
postProcessAfterInstantiation：实例化Bean之后，相当于new这个bean之后
postProcessPropertyValues：bean实例化完成，属性注入阶段触发，@Autowired @Resource等注解原理基于此方法实现
postProcessBeforeInitialization：初始化bean之前，相当于把bean注入spring上下文之前
postProcessAfterInitialization：初始化bean之后，相当于把bean注入spring上下文之后
该拓展点非常有用，无论是写中间件和业务中，都能利用这个特性，比如实现某一类接口的bean在各个生命期间进行收集，或者对某个类型的bean进行同意的设置等

**org.springframework.beans.factory.config.SmartInstantiationAwareBeanPostProcessor**
predictBeanType：发生在postProcessBeforeInstantiation（new这个bean之前）之前，用于预测bean的类型，返回第一个预测成功的Class类型，如果不能预测返回null
determineCandidateConstructors：发生在postProcessBeforeInstantiation之后，用于确定该bean的构造函数，返回该Bean的所有构造函数列表，可以拓展此点自定义选择相应的构造起来实例化这个bean
getEarlyBeanReference：发生在postProcessAfterInstantiation （new这个bean之后）之后，当有循环依赖的场景，当bean实例化好之后，为了防止循环依赖，会提前暴露此回调方法，用于bean实例化的后置处理。

**org.springframework.beans.factory.BeanFactoryAware**
bean实例化之后，属性注入之前
void setBeanFactory(BeanFactory beanFactory) 
使用场景为：缓存beanFactory 用于后续利用beanFactory 来获取和操作容器中的bean

**org.springframework.context.support.ApplicationContextAwareProcessor**
该类本身并没有扩展点，但是该类内部却有6个扩展点可供实现 ，这些类触发的时机在bean实例化之后，初始化之前
**EnvironmentAware**：用于获取Enviroment 的一个拓展类，非常有用，可以获取系统内所有参数。当然在spring内部也可以通过注入的方式直接获得此实例
**EmbeddedValueResolverAware**：用于获取StringValueResolver 的一个拓展类，StringValueResolver可以用于获取String类型的properties变量，一般我们都用@Value获取，可以通过实现这个Aware接口，将StringValueResolver缓存起来用于获取String类型的变量，效果相同

**ResourceLoaderAware**：用于获取ResourceLoader的一个拓展类，ResourceLoader用于获取classpath内的所有资源对象，可以拓展此类拿到ResourceLoader对象

**ApplicationEventPublisherAware**：用于获取ApplicationEventPublisher的一个拓展类，ApplicationEventPublisher用来发布时间，结合ApplicationListener来共同使用

**MessageSourceAware**：用于获取MessageSource的一个拓展类，MessageSource主要用来做国际化

**ApplicationContextAware**：用来获取ApplicationContext的一个拓展类，ApplicationContext 是spring上下文管理器，可以手动获取spring上下文注册的任何bean，可以拓展这个接口来缓存spring上下文，包装成静态方法。同时，ApplicationContext也实现了BeanFactory、MessageSource、ApplicationEventPublisher等接口。



**org.springframework.beans.factory.BeanNameAware#setBeanName**

触发点在bean初始化之前，也就是 postProcessBeforeInitialization之前 ，可以自行修改beanName值



**javax.annotation.PostConstruct**

触发点在postProcessBeforeInitialization之后，InitializingBean.afterPropertiesSet之前



**org.springframework.beans.factory.InitializingBean#afterPropertiesSet**

为bean提供了初始化方法，凡是实现改接口的类在初始化bean的时候都会执行到这个方法，执行时机为postProcessAfterInitialization之前

使用场景：用户实现此接口，来进行系统启动的时候一些业务指标的初始化工作



**org.springframework.beans.factory.FactoryBean**

当往容器中注入class为FactoryBean类型的时候（也就是实现了FactoryBean接口）最终生成的bean是通过FactoryBean的getObject获取的

比较重要的拓展点，很多开源框架都使用过，例如MyBatis @MapperScan注解的作用是将每个接口对应的MapperFactoryBean注册到Spring容器的

一般来说，FactoryBean适合比较复杂Bean的构建，开源框架中整合spring用的比较多



**org.springframework.beans.factory.SmartInitializingSingleton**

接口只有一个方法：afterSingletonsInstantiated ，其作用是在spring容器管理的所有单例对象（非懒加载对象）初始化完成之后调用的回调接口，触发时机在postProcessAfterInitialization之后

使用场景是所有单例对象初始化后做一些后置工作



**org.springframework.boot.CommandLineRunner**

接口只有一个方法：run(String... args)，触发时机是整个项目启动后执行，可利用@Order排序



**org.springframework.beans.factory.DisposableBean**

destroy()方法，对象销毁前执行，比如说运行`applicationContext.registerShutdownHook`时，就会触发这个方法



**org.springframework.context.ApplicationListener**

监听某个事件的event，可以自定义event事件实现发布订阅模式

spring内置事件：

**ContextRefreshedEvent** ApplicationContext初始化或刷新时发布此事件，初始化是指：所有的Bean被成功装载，后处理Bean被检测并激活，所有Singleton Bean 被预实例化，`ApplicationContext`容器已就绪可用

**ContextStartedEvent** ApplicationContext run方法执行时

**ContextStoppedEvent** ApplicationContext stop方法执行时，做一些必要的清理工作

**ContextClosedEvent** 当使用 `ConfigurableApplicationContext`接口中的 `close()`方法关闭 `ApplicationContext` 时，该事件被发布。一个已关闭的上下文到达生命周期末端；它不能被刷新或重启

**RequestHandledEvent** 这是一个 web-specific 事件，告诉所有 bean HTTP 请求已经被服务。只能应用于使用DispatcherServlet的Web应用。在使用Spring作为前端的MVC控制器时，当Spring处理用户请求结束后，系统会自动触发该事件


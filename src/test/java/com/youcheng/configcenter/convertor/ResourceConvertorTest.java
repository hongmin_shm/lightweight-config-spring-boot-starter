package com.youcheng.configcenter.convertor;

import com.youcheng.configcenter.factory.ResourceConvertorFactory;
import org.junit.Before;
import org.junit.Test;
import java.util.Properties;

public class ResourceConvertorTest {

    @Before
    public void setUp() {
    }

    @Test
    public void convertToProperties() {
        Properties properties = ResourceConvertorFactory.getResourceConvertorInstance(".properties").convertToProperties(properties());
        System.out.println(properties);
    }

    @Test
    public void convertToProperties2() {
        Properties properties = ResourceConvertorFactory.getResourceConvertorInstance(".json").convertToProperties(json());
        System.out.println(properties);
    }

    @Test
    public void convertToProperties3() {
        System.out.println(yaml());
        Properties properties = ResourceConvertorFactory.getResourceConvertorInstance(".yml").convertToProperties(yaml());
        System.out.println(properties);
    }

    private String properties() {
        return "user.name=helloworld\nuser.email=raycloud.com";
    }

    private String json() {
        return "{\"user.name\":\"zhangsan\",\"user.emal\":\"rayclod.com\"}";
    }

    private String yaml() {
        return "user:\n  name: zhangsan\n  emal: raycloud.com";
    }
}
package com.youcheng.configcenter.dao;

import com.youcheng.configcenter.exception.ConfigCenterServiceException;
import com.youcheng.configcenter.model.ConfigFileDO;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.List;

public class ApplicationConfigDaoTest {

    private ApplicationConfigDao applicationConfigDao;

    @Before
    public void setUp() {
        DataAccessBase dataAccessBase = new DataAccessBase("com.mysql.cj.jdbc.Driver", "jdbc:mysql://localhost:3306/config_center", "root", "mysql123");
        applicationConfigDao = new ApplicationConfigDaoImpl(dataAccessBase);
    }

    @Test
    public void saveConfigFile() throws ConfigCenterServiceException {
        ConfigFileDO configFileDO = new ConfigFileDO();
        configFileDO
                .setFileId("abcdefg")
                .setName("application")
                .setExtension("properties")
                .setProfile("local")
                .setContent("server.name=config_center_hello_world")
                .setLastUpdatedUser("system")
                .setLastUpdatedTime(new Date());
        applicationConfigDao.saveConfigFile(configFileDO);
    }

    @Test
    public void findConfigFileLast() throws ConfigCenterServiceException {
        ConfigFileDO configFileLast = applicationConfigDao.findConfigFileLast("abcdefg", "local");
        System.out.println(configFileLast.toString());
    }

    @Test
    public void findConfigFile() throws ConfigCenterServiceException {
        ConfigFileDO configFile = applicationConfigDao.findConfigFile("abcdefg", "local", 1);
        System.out.println(configFile);
    }

    @Test
    public void listConfigFileHistory() throws ConfigCenterServiceException {
        List<ConfigFileDO> configFileDOS = applicationConfigDao.listConfigFileHistory("abcdefg", "local");
        System.out.println(configFileDOS);
    }
}
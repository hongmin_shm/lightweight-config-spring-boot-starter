package com.youcheng.configcenter.exception;

import lombok.Getter;

/**
 * @author sunhongmin
 * @date 2022/10/22 09:30
 * @description
 */
@Getter
public class ConfigCenterServiceException extends Exception {
    private Object object;

    public ConfigCenterServiceException(String message) {
        super(message);
    }

    public ConfigCenterServiceException(String message, Object object) {
        super(message);
        this.object = object;
    }

    public ConfigCenterServiceException(Throwable cause) {
        super(cause);
    }
}

package com.youcheng.configcenter.model;

import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sunhongmin
 * @date 2022/10/24 20:04
 * @description 单例 文件基础信息本地存储
 */
@Getter
public enum ConfigFileStoreEnum {

    /**
     * 配置组集合Map存储单例
     */
    LOCAL_CONFIG_FILE_STORE(new HashMap<>(16));

    /**
     * 配置组集合map
     */
    private final Map<String, List<ConfigFileDO>> configGroupMap;
    /**
     * 默认组名
     */
    public final static String DEFAULT_GROUP_NAME = "defaultGroup";

    ConfigFileStoreEnum(Map<String, List<ConfigFileDO>> configGroupMap) {
        this.configGroupMap = configGroupMap;
    }
}

package com.youcheng.configcenter.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author sunhongmin
 * @date 2022/10/21 17:40
 * @description 配置文件data object
 */
@Data
@Accessors(chain = true)
public class ConfigFileDO {

    private Long id;

    /**
     * 文件唯一ID
     */
    private String fileId;

    /**
     * 文件名称
     */
    private String name;

    /**
     * 环境
     */
    private String profile;

    /**
     * 后缀 properties / yaml / json
     */
    private String extension;

    /**
     * 内容
     */
    private String content;

    /**
     * 最后更新时间
     */
    private Date lastUpdatedTime;

    /**
     * 更新操作人
     */
    private String lastUpdatedUser;

    /**
     * 版本
     */
    private Long version;

}

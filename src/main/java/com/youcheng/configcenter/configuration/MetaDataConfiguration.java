package com.youcheng.configcenter.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author sunhongmin
 * @date 2022/10/23 17:21
 * @description
 */
@Configuration
@ConfigurationProperties(prefix = "lightweight")
@Data
public class MetaDataConfiguration {

    private Config config;

    @Data
    @Configuration
    @ConfigurationProperties(prefix = "lightweight.config")
    public static class Config {
        /**
         * 配置加载实现类全路径
         */
        private String provider;
        private DataBase dataBase;
        private ConfigFile configFile;
        private Gitee gitee;
        private RefreshConfig refresh;
    }

    @Data
    @Configuration
    @ConfigurationProperties(prefix = "lightweight.config.data-base")
    public static class DataBase {
        /**
         * JDBC驱动
         */
        private String driverClassName;
        /**
         * 数据库链接
         */
        private String url;
        /**
         * DB username
         */
        private String username;
        /**
         * DB password
         */
        private String password;
    }

    @Data
    @Configuration
    @ConfigurationProperties(prefix = "lightweight.config.gitee")
    public static class Gitee {
        /**
         * git 文件访问链接，参考gitee developer
         */
        private String url;
        /**
         * git access_token
         */
        private String token;
    }

    @Data
    @Configuration
    @ConfigurationProperties(prefix = "lightweight.config.configfile")
    public static class ConfigFile {
        /**
         * 配置文件ID
         */
        private String id;
        /**
         * 环境
         */
        private String profile;
    }

    @Data
    @Configuration
    @ConfigurationProperties(prefix = "lightweight.config.refresh")
    public static class RefreshConfig {
        /**
         * 是否开启定时刷新
         */
        private boolean enable;
        /**
         * 刷新间隔，单位秒
         */
        private Long delay = 60 * 30L;
    }
}

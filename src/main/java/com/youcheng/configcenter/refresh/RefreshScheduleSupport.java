package com.youcheng.configcenter.refresh;

import com.youcheng.configcenter.boot.RefreshConfigExecutor;
import com.youcheng.configcenter.exception.ConfigCenterServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * @author sunhongmin
 * @date 2022/10/23 21:48
 * @description 配置刷新执行器
 */
@Slf4j
@Component
public class RefreshScheduleSupport implements EnvironmentAware {
    public Environment environment;
    @Autowired
    private RefreshConfigExecutor refreshConfigExecutor;

    public void doRefreshExcute() throws ConfigCenterServiceException {
        log.info("doing lightweight config refresh task ...");
        refreshConfigExecutor.execute();
    }


    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}

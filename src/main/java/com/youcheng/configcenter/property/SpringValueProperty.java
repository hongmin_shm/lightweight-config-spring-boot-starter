package com.youcheng.configcenter.property;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * @author sunhongmin
 */
@Data
@AllArgsConstructor
public class SpringValueProperty implements Serializable {
    private static final long serialVersionUID = -8956519848116609175L;
    /**
     * @Value注解中的表达式
     */
    private String express;
    /**
     * 表达式key
     */
    private String key;
    /**
     * 当前配置值
     */
    private String value;
    /**
     * 标记@Value的beanname
     */
    private String beanName;
    /**
     * 标记@Value的bean
     */
    private Object bean;
    /**
     * 标记@Value的字段
     */
    private Field field;

}

package com.youcheng.configcenter.dao;

import com.youcheng.configcenter.exception.ConfigCenterServiceException;
import com.youcheng.configcenter.model.ConfigFileDO;

import java.util.List;

/**
 * @author sunhongmin
 * @date 2022/10/21 23:55
 * @description
 */
public interface ApplicationConfigDao {

    /**
     * 新增配置
     * @param configFileDO
     * @throws ConfigCenterServiceException
     */
    void saveConfigFile(ConfigFileDO configFileDO) throws ConfigCenterServiceException;

    /**
     * 获取配置最新版本
     * @param fileId ID
     * @param profile 环境
     * @return
     * @throws ConfigCenterServiceException
     */
    ConfigFileDO findConfigFileLast(String fileId, String profile) throws ConfigCenterServiceException;

    /**
     * 获取配置指定版本
     * @param fileId ID
     * @param profile 环境
     * @param version 版本
     * @return 指定版本的配置内容
     * @throws ConfigCenterServiceException
     */
    ConfigFileDO findConfigFile(String fileId, String profile, Integer version) throws ConfigCenterServiceException;

    /**
     * 获取配置历史版本列表
     * @param fileId ID
     * @param profile 环境
     * @return
     * @throws ConfigCenterServiceException
     */
    List<ConfigFileDO> listConfigFileHistory(String fileId, String profile) throws ConfigCenterServiceException;

}

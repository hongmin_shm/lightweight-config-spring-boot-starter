package com.youcheng.configcenter.boot;

import com.youcheng.configcenter.configuration.MetaDataConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author sunhongmin
 */
@Configuration
@Import(MetaDataConfiguration.class)
public class RemoteConfigAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public SpringValueAnnotationProcessor springValueAnnotationProcessor() {
        return new SpringValueAnnotationProcessor();
    }

    @Bean
    @ConditionalOnMissingBean
    public RefreshConfigExecutor refreshConfigExecutor() {
        return new RefreshConfigExecutor();
    }

}

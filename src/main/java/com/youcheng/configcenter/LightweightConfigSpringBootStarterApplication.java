package com.youcheng.configcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author sunhongmin
 */
@SpringBootApplication
public class LightweightConfigSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(LightweightConfigSpringBootStarterApplication.class, args);
    }

}

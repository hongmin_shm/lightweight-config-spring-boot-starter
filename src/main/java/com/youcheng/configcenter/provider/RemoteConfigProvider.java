package com.youcheng.configcenter.provider;

import java.util.Properties;

/**
 * @author sunhongmin
 */
public interface RemoteConfigProvider {

    Properties load();

}

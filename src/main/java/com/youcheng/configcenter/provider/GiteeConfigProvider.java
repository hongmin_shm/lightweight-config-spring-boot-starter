package com.youcheng.configcenter.provider;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.youcheng.configcenter.factory.ResourceConvertorFactory;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * @author sunhongmin
 */
public class GiteeConfigProvider implements RemoteConfigProvider {

    /**
     * Gitee config repository remote URL
     *
     * @example https://gitee.com/api/v5/repos/{owner}/{repo}/contents(/{path})
     * @describe <a href="https://gitee.com/api/v5/swagger#/getV5ReposOwnerRepoContents%28Path%29">参考gitee developer文档</a>
     */
    private final String url;

    /**
     * Gitee Access Token
     */
    private final String token;

    private final OkHttpClient httpClient;

    private static final String PROPERTIES_KEY = "content";

    public GiteeConfigProvider(String url, String token) {
        this.url = url;
        this.token = token;
        // only init one connection
        this.httpClient = new OkHttpClient.Builder()
                .connectionPool(new ConnectionPool(1, 5L, TimeUnit.MINUTES))
                .build();
    }

    @Override
    public Properties load() {

        Request request = new Request.Builder()
                .url(String.format("%s?access_token=%s", this.url, this.token))
                .get()
                .build();

        try (Response response = this.httpClient.newCall(request).execute()) {
            if (response.isSuccessful()) {
                assert response.body() != null;
                Map data = new ObjectMapper().readValue(response.body().string(), Map.class);
                String config = data.get(PROPERTIES_KEY).toString();
                config = new String(Base64.getDecoder().decode(config), StandardCharsets.UTF_8.name());
                return ResourceConvertorFactory.getResourceConvertorInstance(this.url).convertToProperties(config);
            } else {
                assert response.body() != null;
                System.err.printf("读取Gitee配置文件发生错误[%s]%n", response.body().string());
            }
        } catch (IOException e) {
            System.err.println("读取Gitee配置文件发生错误");
            e.printStackTrace();
        }

        return null;
    }

}

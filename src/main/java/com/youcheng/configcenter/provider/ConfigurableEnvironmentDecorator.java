package com.youcheng.configcenter.provider;

import com.youcheng.configcenter.exception.ConfigCenterServiceException;
import lombok.Data;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.StringUtils;

/**
 * @author sunhongmin
 * @date 2022/10/23 10:17
 * @description
 */
public class ConfigurableEnvironmentDecorator {

    private final ConfigurableEnvironment environment;
    public static final String PROVIDER_KEY = "lightweight.config.provider";

    public ConfigurableEnvironmentDecorator(ConfigurableEnvironment environment) {
        this.environment = environment;
    }

    public String getRemoteConfigProvider() {
        return environment.getProperty(PROVIDER_KEY);
    }

    public GiteeConfigBean loadGiteeConfigBean() throws ConfigCenterServiceException {
        GiteeConfigBean giteeConfigBean = new GiteeConfigBean();
        giteeConfigBean.setUrl(getPropertyAndCheck(GiteeConfigBean.GITEE_URL_KEY));
        giteeConfigBean.setToken(getPropertyAndCheck(GiteeConfigBean.GITEE_TOKEN_KEY));
        return giteeConfigBean;
    }

    public DataBaseConfigBean loadDataBaseConfigBean() throws ConfigCenterServiceException {
        DataBaseConfigBean dataBaseConfigBean = new DataBaseConfigBean();
        dataBaseConfigBean.setDriverClassName(getPropertyAndCheck(DataBaseConfigBean.DRIVER_CLASS_NAME_KEY));
        dataBaseConfigBean.setUrl(getPropertyAndCheck(DataBaseConfigBean.JDBC_URL_KEY));
        dataBaseConfigBean.setUserName(getPropertyAndCheck(DataBaseConfigBean.JDBC_USERNAME_KEY));
        dataBaseConfigBean.setPassword(getPropertyAndCheck(DataBaseConfigBean.JDBC_PASSW_KEY));
        return dataBaseConfigBean;
    }

    public ConfigFileConfigBean loadConfigFileConfigBean() throws ConfigCenterServiceException {
        ConfigFileConfigBean configFileConfigBean = new ConfigFileConfigBean();
        configFileConfigBean.setFileId(getPropertyAndCheck(ConfigFileConfigBean.CONFIG_FILE_ID));
        configFileConfigBean.setProfile(getPropertyAndCheck(ConfigFileConfigBean.CONFIG_FILE_PROFILE));
        return configFileConfigBean;
    }

    public String getPropertyAndCheck(String configKey) throws ConfigCenterServiceException {
        String propertyValue = environment.getProperty(configKey);
        if (StringUtils.hasLength(propertyValue)) {
            return propertyValue;
        } else {
            throw new ConfigCenterServiceException("获取指定配置信息失败，key=" + configKey);
        }
    }


    @Data
    public static class GiteeConfigBean {
        public static final String PROVIDER = "com.youcheng.configcenter.provider.GiteeConfigProvider";
        private static final String GITEE_URL_KEY = "lightweight.config.gitee.url";
        private static final String GITEE_TOKEN_KEY = "lightweight.config.gitee.token";
        private String url;
        private String token;
    }

    @Data
    public static class DataBaseConfigBean {
        public static final String PROVIDER = "com.youcheng.configcenter.provider.DataBaseConfigProvider";
        private final static String DRIVER_CLASS_NAME_KEY = "lightweight.config.database.driver-class-name";
        private final static String JDBC_URL_KEY = "lightweight.config.data-base.url";
        private final static String JDBC_USERNAME_KEY = "lightweight.config.data-base.username";
        private final static String JDBC_PASSW_KEY = "lightweight.config.data-base.password";
        private String driverClassName;
        private String url;
        private String userName;
        private String password;
    }

    @Data
    public static class ConfigFileConfigBean{
        private final static String CONFIG_FILE_ID = "lightweight.config.configfile.id";
        private final static String CONFIG_FILE_PROFILE = "lightweight.config.configfile.profile";
        private String fileId;
        private String profile;
    }

}

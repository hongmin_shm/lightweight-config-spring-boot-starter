package com.youcheng.configcenter.convertor;

import com.google.gson.Gson;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Properties;

/**
 * @author sunhongmin
 * @date 2022/10/23 14:42
 * @description
 */
public class JsonResourceConvertor implements ResourceConvertor {
    @Override
    public Properties convertToProperties(String content) {
        Properties properties = new Properties();
        if (!StringUtils.hasLength(content)) {
            return null;
        }
        Gson gson = new Gson();
        Map<String, String> map = gson.fromJson(content, Map.class);
        map.forEach(properties::setProperty);
        return properties;
    }
}

package com.youcheng.configcenter.convertor;

import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

/**
 * @author sunhongmin
 * @date 2022/10/23 14:41
 * @description
 */
public class PropertiesResourceConvertor implements ResourceConvertor {
    @Override
    public Properties convertToProperties(String content) {
        Properties properties = new Properties();
        try {
            properties.load(new StringReader(content));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return properties;
    }
}

package com.youcheng.configcenter.convertor;

import java.util.Properties;

/**
 * @author sunhongmin
 * @date 2022/10/23 14:38
 * @description
 */
public interface ResourceConvertor {
    /**
     * 配置格式转换
     * @param content 配置内容
     * @return properties
     */
    Properties convertToProperties(String content);
}
